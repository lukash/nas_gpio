import spidev
import time

ACS712_05B_SENSITIVITY = 185
VREF = 5000
ZERO_OFFSET = 2.500

AIN0_config = [0b11000001, 0b00001011]  # 5V voltage
AIN1_config = [0b11010001, 0b00001011]  # 5V current
AIN2_config = [0b11100001, 0b00001011]  # 12V voltage
AIN3_config = [0b11110001, 0b00001011]  # 12V current

fullscale = 6.144

# Measured coefficient of full voltage
AIN0_coefficient = 1.977  # 5.05V == 2.554V [470k/680k]
AIN1_coefficient = 1.480  # 2.50V == 1.684V [470k/680k]
AIN2_coefficient = 4.892  # 12.48V == 2.551V [1M/300k]
AIN3_coefficient = 1.500  # 2.54V == 1.684V  [470k/480k]


''' Power information retrieved by reading AD converter 
    ADS1118 via SPI interface.
    ADC has 4 channels. 2 channels are measuring voltage,
    other 2 channels are measuring current via ACS712 sensor 
'''
class PowerInfo:

    def __init__(self):
        self.adc = spidev.SpiDev()
        self.adc.open(0, 0)  # open(X,Y) will open /dev/spidev-X.Y
        self.adc.max_speed_hz = 1000000
        self.adc.mode = 0x01

        #adc.close()

    def _convert_to_signed_int(self, high_byte, low_byte):
        num = high_byte * 256 + low_byte
        if num > (2**15 - 1):
            return (2**16 - num) * (-1)
        else:
            return num

    def print_bits(self, bytes):
        for b in bytes:
            print('{0:08b}'.format(b))

    def _read_voltage(self, config, coeficient):
        time.sleep(0.15)
        req = config + config
        result = [0x0, 0x0, 0x0, 0x0]
        max_retry = 5
        while result[3] != config[1] or max_retry == 0:
            max_retry -= 1
            # send config to start measurement
            self.adc.xfer2(req)  # ignore returned value
            # wait some time, until measuremnt is done
            time.sleep(0.15)
            # to read back value stored in register, we have to send some value,
            # so SPI will generage clock cycles which ADC chip will use to transfer data back
            result = self.adc.xfer2([0x0, 0x0, 0x0, 0x0])
            time.sleep(0.15)

        if max_retry == 0:
            return -1
        else:
            result_int = self._convert_to_signed_int(result[0], result[1])
            # print('Raw result: %s' % result_int)
            voltage = result_int * fullscale / 32767 * coeficient
            # print('Voltage: %sV' % voltage)
            return voltage

    def _read_current(self, config, coeficient, supply_voltage):
        # Execute 3 measurements for better precision
        voltage = 0
        measurement_count = 3
        for i in range(0, measurement_count):
            voltage += self._read_voltage(config, coeficient)
        voltage /= measurement_count
        diff_voltage = (voltage - (supply_voltage/2))  # in V
        current = abs(diff_voltage * 1000 / ACS712_05B_SENSITIVITY)  # in A
        return current

    def read_power(self):
        voltage5V = self._read_voltage(AIN0_config, AIN0_coefficient)
        current5V = self._read_current(AIN1_config, AIN1_coefficient, voltage5V)

        voltage12V = self._read_voltage(AIN2_config, AIN2_coefficient)
        current12V = self._read_current(AIN3_config, AIN3_coefficient, voltage5V)

        return (voltage5V, current5V, voltage12V, current12V)
