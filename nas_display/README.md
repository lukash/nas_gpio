NAS display
===========
SSD1306 display for NAS showing system info.

Install
-------
Make sure i2c is enabled and module is loaded.

    virtualenv ./virtualenv
    source virtualenv/bin/activate
    pip install -r requirements.txt
    cp -r nas_display /opt/nas_display 

Systemd service
---------------

    cp display.service /etc/systemd/system/display.service

Create font
-----------
Create bitmap font for display:

  fontforge > Export font as bdf 
  bdftopcf -o v-9.pcf v-9.bdf 
  python pilfont.py v-9.pcf
