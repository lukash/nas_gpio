#!/usr/bin/python3

from PIL import Image,ImageFont, ImageDraw

from power_info import PowerInfo
from system_info import SysInfo, NetInfo, DiskInfo
from threading import Thread
from gpiod.line import Direction, Value, Bias
from time import sleep
import gpiod
import traceback
import time
import multiprocessing
import os
import sys


class Screen:
    def reload_data(self):
        pass

    def draw(self):
        pass


class HddScreen(Screen):

    def __init__(self, display):
        self.canvas = display.canvas
        self.font = display.font
        self.font2 = display.font2
        self.disk1Info = None
        self.disk2Info = None

        self.disk1_available = False
        self.disk1_used_space = 0
        self.disk1_temp = 0
        self.disk1_speed = 0

        self.disk2_available = False
        self.disk2_used_space = 0
        self.disk2_temp = 0
        self.disk2_speed = 0
        self.started = False

    def reload_data(self):
        if not self.disk1Info:
            self.disk1Info = DiskInfo(0)
        if not self.disk2Info:
            self.disk2Info = DiskInfo(2)

        self.disk1_available = self.disk1Info.disk_available()
        self.disk1_used_space = self.disk1Info.disk_space_used()
        self.disk1_temp = self.disk1Info.disk_temp()
        self.disk1_speed = self.disk1Info.disk_speed()

        self.disk2_available = self.disk2Info.disk_available()
        self.disk2_used_space = self.disk2Info.disk_space_used()
        self.disk2_temp = self.disk2Info.disk_temp()
        self.disk2_speed = self.disk2Info.disk_speed()

    def draw(self):
        self.canvas.text((0, 18), '1',  font=self.font2, fill=1)
        self.canvas.text((12, 18), '[',  font=self.font2, fill=1)
        self.canvas.text((83, 18), ']',  font=self.font2, fill=1)

        if self.disk1_available:
            space_used1 = str(int(self.disk1_used_space)) + '%'
            self.canvas.text((95, 18), str(self.disk1_temp) + '°C', font=self.font, fill=1)
            Elements.draw_progress(self.canvas, (20, 20, 82, 29), self.disk1_speed)
        else:
            space_used1 = 'N/A'
            self.canvas.text((40, 18), 'N/A',  font=self.font2, fill=1)

        self.canvas.text((0, 35), '2',  font=self.font2, fill=1)
        self.canvas.text((12, 35), '[',  font=self.font2, fill=1)
        self.canvas.text((83, 35), ']',  font=self.font2, fill=1)
        if self.disk2_available:
            space_used2 = str(int(self.disk2_used_space)) + '%'
            self.canvas.text((95, 137), str(self.disk2_temp) + '°C',  font=self.font, fill=1)
            Elements.draw_progress(self.canvas, (20, 37, 82, 46), self.disk2_speed)
        else:
            space_used2 = 'N/A'
            self.canvas.text((40, 35), 'N/A',  font=self.font2, fill=1)

        self.canvas.text((0, 52), 'Used: ' + space_used1 + ' / ' + space_used2,  font=self.font, fill=1)


class NetScreen(Screen):

    def __init__(self, display):
        self.canvas = display.canvas
        self.font = display.font
        self.font2 = display.font2
        self.netInfo = None

        self.net_ip = 'Loading...'
        self.net_upload = '0'
        self.net_download = '0'

    def reload_data(self):
        if not self.netInfo:
            self.netInfo = NetInfo()

        self.net_ip = self.netInfo.net_ip()
        self.net_upload = self.netInfo.net_upload()
        self.net_download = self.netInfo.net_download()

    def draw(self):
        self.canvas.text((0, 18), '',  font=self.font, fill=1)

        self.canvas.text((0, 18), self.net_ip, font=self.font, fill=1)

        self.canvas.text((0, 35), 'UP:',  font=self.font2, fill=1)
        self.canvas.text((30, 35), self.net_upload, font=self.font, fill=1)

        self.canvas.text((0, 52), 'DL:',  font=self.font2, fill=1)
        self.canvas.text((30, 52), self.net_download, font=self.font, fill=1)


class SysScreen(Screen):

    def __init__(self, display):
        self.canvas = display.canvas
        self.font = display.font
        self.font2 = display.font2
        self.sysInfo = None

        self.uptime = 'Loading...'
        self.cpu_temp = 0
        self.cpu_usage = 0
        self.mem_usage = 0

    def reload_data(self):
        if not self.sysInfo:
            self.sysInfo = SysInfo()

        self.uptime = self.sysInfo.sys_uptime()
        self.cpu_temp = self.sysInfo.sys_cpu_temp()
        self.cpu_usage = self.sysInfo.sys_cpu_usage()
        self.mem_usage = self.sysInfo.sys_mem_usage()

    def draw(self):
        self.canvas.text((0, 18), 'Up: ' + self.uptime, font=self.font, fill=1)

        self.canvas.text((0, 35), 'CPU',  font=self.font, fill=1)
        self.canvas.text((30, 35), '[',  font=self.font2, fill=1)
        self.canvas.text((83, 35), ']',  font=self.font2, fill=1)
        self.canvas.text((95, 35), '',  font=self.font, fill=1)

        self.canvas.text((95, 35), str(int(self.cpu_temp)) + '°C', font=self.font, fill=1)

        Elements.draw_progress(self.canvas, (38, 37, 82, 46), self.cpu_usage)

        self.canvas.text((0, 52), 'RAM',  font=self.font, fill=1)
        self.canvas.text((30, 52), '[',  font=self.font2, fill=1)
        self.canvas.text((83, 52), ']',  font=self.font2, fill=1)

        Elements.draw_progress(self.canvas, (38, 54, 82, 63), self.mem_usage)


class PwrScreen(Screen):

    def __init__(self, display):
        self.display = display
        self.canvas = display.canvas
        self.font = display.font
        self.font2 = display.font2
        self.pwrInfo = None
        self.data_initialized = False

        self.voltage5v = 5.0
        self.current5v = 0
        self.voltage12v = 12.0
        self.current12v = 0

    def reload_data(self):
        if not self.pwrInfo:
            self.pwrInfo = PowerInfo()

        (self.voltage5v, self.current5v, self.voltage12v, self.current12v) = self.pwrInfo.read_power()
        self.data_initialized = True

    def draw(self):
        # print("5V: %.4fV %.4fA" % (voltage5V, current5V))
        # print("12V: %.4fV %.4fA" % (voltage12V, current12V))
        total_power = self.voltage5v * self.current5v + self.voltage12v * self.current12v
        self.canvas.text((0, 18), (' %.2fV %.2fA' % (self.voltage5v, self.current5v)),  font=self.font, fill=1)
        self.canvas.text((0, 35), ('%.2fV %.2fA' % (self.voltage12v, self.current12v)),  font=self.font, fill=1)
        if not self.data_initialized:
            self.canvas.text((0, 52), 'Loading...',  font=self.font, fill=1)
        else:
            self.canvas.text((0, 52), ('Total: %.3fW' % total_power),  font=self.font, fill=1)


class Display:

    SUSPEND = 500
    BOOT_SCREEN = 100
    SHUTDOWN_SCREEN = 200
    TAB_HDD = 0
    TAB_NET = 1
    TAB_SYS = 2
    TAB_PWR = 3

    def __init__(self, draw_lock, selected_tab, output="oled"):
        ''' output: "oled"/"image"
        '''
        self.font = ImageFont.load('fonts/zevv.pil')
        self.font2 = ImageFont.load('fonts/biconf.pil')
        self.width = 128
        self.height = 64
        self.selected_tab = selected_tab
        self.previous_selected_tab = -1
        self.output = output

        self.draw_lock = draw_lock

        if output == "oled":
            self._to_oled()
        elif output == "image":
            self._to_image()

        self.hddScreen = HddScreen(self)
        self.netScreen = NetScreen(self)
        self.sysScreen = SysScreen(self)
        self.pwrScreen = PwrScreen(self)

        self.reload_data_thread = Thread(target=self.reload_tab_data, args=())
        self.reload_data_thread.start()

    def draw(self):
        try:
            self.draw_lock.acquire()
            if self._is_tab_changed() and int(self.selected_tab.value) <= Display.TAB_PWR:
                self._draw_menu()

            if int(self.selected_tab.value) == Display.SUSPEND:
                self._draw_suspend_display()

            if int(self.selected_tab.value) == Display.TAB_HDD:
                self._clear_tab()
                self.hddScreen.draw()
            elif int(self.selected_tab.value) == Display.TAB_NET:
                self._clear_tab()
                self.netScreen.draw()
            elif int(self.selected_tab.value) == Display.TAB_SYS:
                self._clear_tab()
                self.sysScreen.draw()
            elif int(self.selected_tab.value) == Display.TAB_PWR:
                self._clear_tab()
                self.pwrScreen.draw()
            elif int(self.selected_tab.value) == Display.BOOT_SCREEN:
                self._draw_boot_screen()
            elif int(self.selected_tab.value) == Display.SHUTDOWN_SCREEN:
                self._draw_shutdown_screen()

            if self.output == "oled":
                self.oled.display()
            elif self.output == "image":
                filename = "my_drawing.png"
                self.image.save(filename)

            self.previous_selected_tab = self.selected_tab.value
        finally:
            self.draw_lock.release()

    def reload_tab_data(self):
        while True:
            try:
                if int(self.selected_tab.value) == Display.TAB_HDD:
                    self.hddScreen.reload_data()
                elif int(self.selected_tab.value) == Display.TAB_NET:
                    self.netScreen.reload_data()
                elif int(self.selected_tab.value) == Display.TAB_SYS:
                    self.sysScreen.reload_data()
                elif int(self.selected_tab.value) == Display.TAB_PWR:
                    self.pwrScreen.reload_data()
                elif int(self.selected_tab.value) == Display.SUSPEND:
                    time.sleep(5)
            except Exception as e:
                print(e)
                traceback.print_exc()

            time.sleep(1)

    def _is_tab_changed(self):
        return int(self.selected_tab.value) != int(self.previous_selected_tab)

    def _to_oled(self):
        from OLED96 import SSD1306
        from time import sleep
        from PIL import ImageFont
        
        from smbus import SMBus
        try:
            i2cbus = SMBus(2)        # 1 = Raspberry Pi but NOT early REV1 board
            self.oled = SSD1306(i2cbus)   # create oled object, nominating the correct I2C bus, default address
        except:
            i2cbus = SMBus(1)        # Try second one if first one throws exception
            self.oled = SSD1306(i2cbus)   # create oled object, nominating the correct I2C bus, default address
        
        self.canvas = self.oled.canvas;

    def _to_image(self):
        self.image = Image.new('1', (self.width, self.height))
        self.canvas = ImageDraw.Draw(self.image)

    def suspend_display(self):
        self.selected_tab.value = Display.SUSPEND
        self.draw()

    def is_suspended(self):
        return self.selected_tab.value == Display.SUSPEND

    def select_tab(self, tab_index):
        self.selected_tab.value = tab_index

    def next_tab(self):
        self.selected_tab.value += 1
        if int(self.selected_tab.value) > 3:
            self.selected_tab.value = 0
        self.draw()

    def _draw_suspend_display(self):
        self._clear_screen()

    def _draw_menu(self):
        tab_index = int(self.selected_tab.value)
        offset = [0, 30, 60, 90]
        self.canvas.rectangle((0, 0, self.width, 15), outline=0, fill=0)
        self.canvas.rectangle((offset[tab_index], 0, offset[tab_index] + 28, 15), outline=1, fill=1)

        self.canvas.text((offset[0] + 2, 0), 'HDD',  font=self.font, fill=(0 if tab_index == 0 else 1))
        self.canvas.text((offset[1] + 2, 0), 'Net',  font=self.font, fill=(0 if tab_index == 1 else 1))
        self.canvas.text((offset[2] + 2, 0), 'Sys',  font=self.font, fill=(0 if tab_index == 2 else 1))
        self.canvas.text((offset[3] + 2, 0), 'Pwr',  font=self.font, fill=(0 if tab_index == 3 else 1))
        self.canvas.line((0, 15, self.width, 15), fill=1)

    def _clear_tab(self):
        self.canvas.rectangle((0, 16, self.width, self.height), outline=0, fill=0)

    def _clear_screen(self):
        self.canvas.rectangle((0, 0, self.width, self.height), outline=0, fill=0)

    def _draw_boot_screen(self):
        self._clear_screen()
        self.canvas.text((35, 30), '[ NAS ]',  font=self.font2, fill=1)

    def _draw_shutdown_screen(self):
        self._clear_screen()
        self.canvas.text((0, 25), 'Shutting down...',  font=self.font, fill=1)


class Elements:

    @staticmethod
    def draw_progress(canvas, max_xy, length_percentage):
        max_length = max_xy[2] - max_xy[0]
        cur_end_x = max_xy[0] + int(int(length_percentage) * max_length / 100)
        canvas.rectangle((max_xy[0], max_xy[1], cur_end_x, max_xy[3]), outline=1, fill=1)


def sleep_if_necessary(start, end, seconds):
    elapsed = end - start
    to_sleep = seconds - elapsed
    if to_sleep > 0:
        time.sleep(to_sleep)

PIN_BTN = 27  # Replace with the actual GPIO pin number

def open_gpio():
    return gpiod.request_lines(
        "/dev/gpiochip0",
        consumer="btn-nas-display",
        config={
            tuple([27]): gpiod.LineSettings(
                direction=Direction.INPUT,
                bias=Bias.PULL_UP
            )
        },
    )

BTN_PRESSED = Value.INACTIVE

def wait_for_button_press(request):
    val = request.get_values()[0]
    while val != BTN_PRESSED:
        val = request.get_values()[0]
        time.sleep(0.1)

    counter = 0
    start = time.time()
    while val == BTN_PRESSED and counter < 120:
        val = request.get_values()[0]
        time.sleep(0.05)
        counter += 1
    end = time.time()
    print("Button pressed for %s seconds" % (end - start))
    return end - start

def handle_button_press(threadName, display):
    with open_gpio() as request:
        while True:
            hold_time = wait_for_button_press(request)
            if 0.05 < hold_time < 4:
                display.next_tab()
            elif hold_time > 5:
                display.select_tab(Display.SHUTDOWN_SCREEN)
                display.draw()
                os.system('sudo systemctl poweroff')


def main():
    draw_lock = multiprocessing.Lock()
    selected_tab = multiprocessing.Value('d', 0)
    d = Display(draw_lock, selected_tab)
    d.select_tab(Display.BOOT_SCREEN)
    d.draw()

    t = multiprocessing.Process(target=handle_button_press, args=('IrqProcess', d, ))
    t.start()
    time.sleep(1)
    d.select_tab(Display.TAB_HDD)

    inactivity_seconds = 0
    sleep_time = 2
    while True:
        if d.is_suspended():
            inactivity_seconds = 0
            time.sleep(10)
            continue

        inactivity_seconds += sleep_time
        if inactivity_seconds > 60:
            d.suspend_display()
            continue

        try:
            start = time.time()
            d.draw()
            end = time.time()
        except Exception as e:
            print('Exception occurred during drawing. ' + str(e))
        finally:
            sleep_if_necessary(start, end, sleep_time)

    t.join()


if __name__ == '__main__':
    programs_dir = os.path.dirname(sys.argv[0])
    if programs_dir != '':
        os.chdir(programs_dir)
    main()
