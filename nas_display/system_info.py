import subprocess
import os
import re
import shutil
import time
from datetime import timedelta

class CpuUsage:

    INTERVAL = 0.3
    def __init__(self):
        pass

    def init(self):
        pass

    def _time_list(self):
        """
        Fetches a list of time units the cpu has spent in various modes
        Detailed explanation at http://www.linuxhowtos.org/System/procstat.htm
        """
        with open("/proc/stat", "r") as stat_file:
            cpu_stats = stat_file.readline()
        columns = cpu_stats.replace("cpu", "").split(" ")
        return map(int, filter(None, columns))
    
    def _delta_time(self, interval):
        """
        Returns the difference of the cpu statistics returned by getTimeList
        that occurred in the given time delta
        """
        timeList1 = self._time_list()
        time.sleep(interval)
        timeList2 = self._time_list()
        return [(t2-t1) for t1, t2 in zip(timeList1, timeList2)]
    
    def cpu_load(self):
        """
        Returns the cpu load as a value from the interval [0.0, 1.0]
        """
        dt = list(self._delta_time(CpuUsage.INTERVAL))
        idle_time = float(dt[3])
        total_time = sum(dt)
        load = 1-(idle_time/total_time)
        return load

class NetUsage:

    def __init__(self, iface_name='eth0'):
        self.iface_name = iface_name
        current_time = int(round(time.time() * 1000)) 
        self.previous_download_time = current_time
        self.previous_upload_time = current_time
        self.previous_download_bytes = 0
        self.previous_upload_bytes = 0

    def init(self):
        self.previous_download_bytes = self._transfered_bytes('rx')
        self.previous_upload_bytes = self._transfered_bytes('tx')

    def _transfered_bytes(self, direction):
        with open('/sys/class/net/' + self.iface_name + '/statistics/' + direction + '_bytes', 'r') as f:
            data = f.read();
        return int(data)

    def upload_speed(self):
        current_time = int(round(time.time() * 1000))
        interval = (current_time - self.previous_upload_time) / 1000
        if interval == 0:
            time.sleep(1)
            return self.upload_speed()

        t1 = self.previous_upload_bytes
        t2 = self._transfered_bytes('tx')
        kilobytes_speed = 0
        kilobytes_speed = (1/interval * (t2-t1))/1024
        self.previous_upload_bytes = t2
        self.previous_upload_time = current_time
        return  '%.2f kB/s' % kilobytes_speed if kilobytes_speed < 2048 else '%.2f MB/s' % (kilobytes_speed/1024)

    def download_speed(self):
        current_time = int(round(time.time() * 1000))
        interval = (current_time - self.previous_download_time) / 1000
        if interval == 0:
            time.sleep(1)
            return self.download_speed()

        t1 = self.previous_download_bytes
        t2 = self._transfered_bytes('rx')
        kilobytes_speed = 0
        kilobytes_speed = (1/interval * (t2-t1))/1024
        self.previous_download_bytes = t2
        self.previous_download_time = current_time
        return  '%.2f kB/s' % kilobytes_speed if kilobytes_speed < 2048 else '%.2f MB/s' % (kilobytes_speed/1024)


class DiskUsage:

    def __init__(self, disk_name='sda'):
        self.disk_name = disk_name
        self.disk_rex = re.compile(r'.*(' + disk_name + ' [^\n]*)', re.DOTALL)
        self.previous_time = int(round(time.time() * 1000)) 
        self.previous_transfered_bytes = 0

    def init(self):
        self.previous_transfered_bytes = self._transfered_bytes();

    def _transfered_bytes(self):
        with open('/proc/diskstats', 'r') as f:
            data = f.read();
        with open('/sys/block/' + self.disk_name  + '/queue/physical_block_size', 'r') as f:
            block_size = f.read();
        match = self.disk_rex.match(data).group(1).strip()
        match = ' '.join(match.split())
        transfered_bytes = (int(match.split()[3]) * int(block_size)) + (int(match.split()[7]) * int(block_size))
        return transfered_bytes

    def _speed(self):
        current_time = int(round(time.time() * 1000))
        interval = (current_time - self.previous_time) / 1000
        if interval == 0:
            time.sleep(1)
            return self._speed()

        t1 = self.previous_transfered_bytes
        t2 = self._transfered_bytes()
        kilobytes_speed = (1/interval * (t2-t1))/1024
        self.previous_transfered_bytes = t2
        self.previous_time = current_time
        return kilobytes_speed

    def speed(self):
        kilobytes_speed = self._speed()
        return  '%.2f kB/s' % kilobytes_speed if kilobytes_speed < 2048 else '%.2f MB/s' % (kilobytes_speed/1024)

    def speed_percentage(self):
        kilobytes_max_speed = 30000;
        kilobytes_cur_speed = self._speed()
        return kilobytes_cur_speed * 100 / kilobytes_max_speed


class SysInfo:

    def __init__(self):
        self.uptime_rex = re.compile(r'.*up([^,]*)', re.DOTALL)
        self.mem_rex = re.compile(r'.*(Mem[^\n\(]*)', re.DOTALL)

    def sys_uptime(self):
        seconds_in_day = 86400
        uptime_string = ''
        with open('/proc/uptime', 'r') as f:
            show_seconds = True
            uptime_seconds = float(f.readline().split()[0])
            if uptime_seconds > seconds_in_day:
                uptime_string += '%dd ' % (uptime_seconds // seconds_in_day)
                uptime_seconds %= seconds_in_day
                show_seconds = False
            uptime_string += '%02d:%02d' % (uptime_seconds // 3600, uptime_seconds % 3600 // 60)
            if show_seconds:
                uptime_string += ':%02d' % (uptime_seconds % 60)
        return uptime_string

    def sys_cpu_usage(self):
        return CpuUsage().cpu_load() * 100 

    def sys_mem_usage(self):
        proc = subprocess.Popen(['free', '-m'], stdout=subprocess.PIPE)
        out = str(proc.stdout.read()).replace('\\n', '\n')
        match = self.mem_rex.match(out).group(1).strip()
        match = ' '.join(match.split())
        return 100 - (int(match.split()[3]) * 100 / int(match.split()[1]))

    def sys_cpu_temp(self):
        with open("/sys/class/thermal/thermal_zone0/temp", "r") as stat_file:
            cpu_temp = stat_file.readline()
        return int(cpu_temp) / 1000


class DiskInfo:

    def __init__(self, disk_index):
        self.disk_index = disk_index
        self.disk_name = 'sda' if disk_index == 0 else 'sdb'
        self.temp_rex = re.compile(r'.*(Temp[^\n\(]*)', re.DOTALL)
        self._disk_available = os.path.isdir('/sys/block/' + self.disk_name)
        if self._disk_available:
            self.disk_usage = DiskUsage(self.disk_name)
            self.disk_usage.init()

    def disk_available(self):
        return self._disk_available

    def disk_speed(self):
        if self._disk_available:
            return self.disk_usage.speed_percentage()
        else:
            return 0

    def disk_space_used(self):
        if self._disk_available:
            mount_point = '/mnt/media'
            disk_space = shutil.disk_usage(mount_point)
            return disk_space.used * 100 / disk_space.total
        else:
            return 0

    def disk_temp(self):
        if self._disk_available:
            proc = subprocess.Popen(['sudo', '/usr/bin/smartctl', '-a', '-d', 'sat', '/dev/' + self.disk_name], stdout=subprocess.PIPE)
            out = str(proc.stdout.read()).replace('\\n', '\n')
            proc.kill()
            match = self.temp_rex.match(out).group(1).strip()
            return match[-2:]
        else:
            return 0


class NetInfo:

    def __init__(self):
        self.ip_rex = re.compile(r'.*inet ([^\s\/]+)', re.DOTALL)
        self.default_iface = self.net_default_iface()
        self.net_usage = NetUsage(self.default_iface)
        self.net_usage.init()

    def net_ip(self):
        proc = subprocess.Popen(['ip', 'addr',  'show', self.default_iface], stdout=subprocess.PIPE)
        out = str(proc.stdout.read()).replace('\\n', '\n')
        match = self.ip_rex.match(out).group(1).strip()
        return match

    def net_upload(self):
        return self.net_usage.upload_speed()

    def net_download(self):
        return self.net_usage.download_speed()

    def net_default_iface(self):
        proc = subprocess.Popen(['route', '-n'], stdout=subprocess.PIPE)
        out = str(proc.stdout.read()).replace('\\n', '\n')
        match = re.match(r'.*0\.0\.0\.0([^\n]+)', out, re.DOTALL).group(1)
        match = ' '.join(match.split())
        return match.split()[-1]

def main():
    di = DiskInfo(0)
    print('Disk temp: ' + str(di.disk_temp()))
    print('Disk space used: ' + str(di.disk_space_used()))
    print('Disk speed: ' + str(di.disk_speed()))
    
    si = SysInfo()
    print('Disk temp: ' + str(di.disk_temp()))
    print('Uptime: ' + str(si.sys_uptime()))
    print('Mem usage: ' + str(si.sys_mem_usage()))
    
    print('CPU temp: ' + str(si.sys_cpu_temp()))
    print('CPU usage: ' + str(si.sys_cpu_usage()))
    
    ni = NetInfo()
    print('Net ip: ' + str(ni.net_ip()))
    print('Net iface: ' + str(ni.net_default_iface()))
    print('Net upload: ' + str(ni.net_upload()))
    print('Net down: ' + str(ni.net_download()))

if __name__ == '__main__':
    main()
