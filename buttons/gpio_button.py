import wiringpi2 as wiringpi
import time

PIN_BTN = 2
PIN_LED = 1

HIGH = 1
LOW = 0

wiringpi.wiringPiSetup()

wiringpi.pinMode(PIN_BTN, wiringpi.GPIO.INPUT)
wiringpi.pinMode(PIN_LED, wiringpi.GPIO.OUTPUT)

while True:
    my_input = wiringpi.digitalRead(PIN_BTN)
    print(my_input) 
    if my_input == LOW:  
        print("Button PRESS") 
        wiringpi.digitalWrite(PIN_LED, HIGH)
    else:  
        print("Button OFF")  
        wiringpi.digitalWrite(PIN_LED, LOW)

    time.sleep(0.5)
