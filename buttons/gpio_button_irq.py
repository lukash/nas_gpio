import wiringpi2 as wiringpi
import time
import os

PIN_BTN = 116
PIN_LED = 88

HIGH = 1
LOW = 0

wiringpi.wiringPiSetupSys() 

wiringpi.pinMode(PIN_BTN, wiringpi.GPIO.INPUT)
wiringpi.pinMode(PIN_LED, wiringpi.GPIO.OUTPUT)

# Prepare GPIO port in /sys/class/devices
if not os.path.isdir('/sys/class/gpio/gpio' + str(PIN_BTN)):
    os.system('gpio edge 116 falling')

LED_STATUS = LOW
def switch_led():
    global LED_STATUS
    print("Button PRESS")
    LED_STATUS = HIGH if LED_STATUS == LOW else LOW
    wiringpi.digitalWrite(PIN_LED, LED_STATUS)

while True:
    # First time it returns immediatelly
    x = wiringpi.waitForInterrupt(PIN_BTN, 50000)
    print(x)
    switch_led()
