HDD Led
=======
Disk read/write indicator using GPIO connected LED.

Compile
--------

    gcc -Wall hddled.c -lgpiod -o hddled

Install
-------

    mkdir /opt/hddled/ 
    cp hddled /opt/hddled/
    cp hddled.service /etc/systemd/system/ 
    systemctl enable hddled

GPIO permisions
---------------

    chmod g+rw /dev/gpiochip0     
    chown root:gpio /dev/gpiochip0
